
<div id="page_container2">
    <!--    <div class="page_header">
            <div class="company">
                <div class="company_logo">
                    <img src="public/images/logo.png" height="15px" width="auto">
                </div>
                <div class="company_name"> 
                    Company Name Company Long Name
                </div>
            </div>
            <div class="information">
                <table width="100%">
                    <tr>
                        <td width="80%">Address: Long Address Sample Long Address Sample Long Address Sample</td>
                        <td>SI No.: 1</td>
                    </tr>
                    <tr>
                        <td>Contact No.: 1234567890</td>
                        <td>Date: 04/24/2018</td>
                    </tr>
                </table>
            </div>
        </div>-->
    <div class="page_header">
        <div class="company2">
            <div class="company_logo2">
                <img src="public/images/logo.png" height="46px" width="auto">
            </div>
            <div class="company_name2"> 
                Company Name Company Long Name
               
            </div>
             <div class="information2">
                    <table>
                        <tr>
                            <td width="80%" class="pL_5">Address: Long Address Sample Long Address Sample Long Address Sample</td>
                            <td class="text-right">SI No.: 1</td>
                        </tr>
                        <tr>
                            <td class="pL_5">Contact No.: 1234567890</td>
                            <td class="text-right">Date: 04/24/2018</td>
                        </tr>
                    </table>
                </div>

        </div>
        <!--        <div class="information2">
                    <table width="100%">
                        <tr>
                            <td width="80%">Address: Long Address Sample Long Address Sample Long Address Sample</td>
                            <td>SI No.: 1</td>
                        </tr>
                        <tr>
                            <td>Contact No.: 1234567890</td>
                            <td>Date: 04/24/2018</td>
                        </tr>
                    </table>
                </div>-->
    </div>
    <div class="table_container">
        <table width="100%">
            <tr>
                <td width="50%">Customer Name: <span>WILCON</span></td>
                <td>Contact No.: <span>1234567</span></td>
            </tr>
            <tr>
                <td>Address: <span>Address Sample Street City</span></td>
                <td>Memo: <span>SAMPLE MEMO MEMO LONG MEMO</span></td>
            </tr>
        </table>
        <!-- 
        Character Length
        Item - 4
        Product Name - 32
        Unit - 6 digit, 1 comma, 1 slash, 3 letter unit
        
        -->
        <table id="item_list">
            <!-- quantity 9, unit price 11, amount 14 -->
            <tr class="thead">
                <td id="col_item">Item</td>
                <td id="col_product_name">Product Name, Color & Size</td>
                <td id="col_quantity">Quantity</td>
                <td id="col_total_pieces">Total Pcs</td>

                <td id="col_unit_price">Unit Price</td>
                <td id="col_amount">Amount</td>
                <td id="col_memo">Memo</td>
            </tr>
            <!-- ROW -->
            <tr>
                <td>1</td>
                <td>ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ<cns>Ultramarine Blue-XL</cns></td>
            <td>88,888 box<unit>(1000pcs)</unit><br>10,000pcs</td>
            <td><num>123,456</num></td>

            <td><num>P456,789.00</num></td>
            <td><num>P56,789,123.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td>2</td>
                <td>Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td>88,888 box<unit>(1000pcs)</unit><br>10,000pcs</td>
            <td><num>123,456</num></td>

            <td><num>P456,789.00</num></td>
            <td><num>P56,789,123.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td>3</td>
                <td>Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td>88,888 box<unit>(1000pcs)</unit><br>10,000pcs</td>
            <td><num>123,456</num></td>

            <td><num>P456,789.00</num></td>
            <td><num>P56,789,123.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td>4</td>
                <td>Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td>88,888 box<unit>(1000pcs)</unit><br>10,000pcs</td>
            <td><num>123,456</num></td>

            <td><num>P456,789.00</num></td>
            <td><num>P56,789,123.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td>5</td>
                <td>Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td>88,888 box<unit>(1000pcs)</unit><br>10,000pcs</td>
            <td><num>123,456</num></td>

            <td><num>P456,789.00</num></td>
            <td><num>P56,789,123.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td>6</td>
                <td>Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td>88,888 box<unit>(1000pcs)</unit><br>10,000pcs</td>
            <td><num>123,456</num></td>

            <td><num>P456,789.00</num></td>
            <td><num>P56,789,123.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td>7</td>
                <td>Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td>88,888 box<unit>(1000pcs)</unit><br>10,000pcs</td>
            <td><num>123,456</num></td>

            <td><num>P456,789.00</num></td>
            <td><num>P56,789,123.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td>8</td>
                <td>Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td>88,888 box<unit>(1000pcs)</unit><br>10,000pcs</td>
            <td><num>123,456</num></td>

            <td><num>P456,789.00</num></td>
            <td><num>P56,789,123.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td>9</td>
                <td>Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td>88,888 box<unit>(1000pcs)</unit><br>10,000pcs</td>
            <td><num>123,456</num></td>

            <td><num>P456,789.00</num></td>
            <td><num>P56,789,123.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td>10</td>
                <td>Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td>88,888 box<unit>(1000pcs)</unit><br>10,000pcs</td>
            <td><num>123,456</num></td>

            <td><num>P456,789.00</num></td>
            <td><num>P56,789,123.00</num></td>
            <td></td>
            </tr>
        </table>
        <table width="768px;">
            <!-- TOTAL ROW TOTAL ROW TOTAL ROW-->
            <tr>
                <td colspan="3" class="text-center" width="250px"><b>Total</b></td>
                <td><b>Pieces / Boxes</b></td>
                <td><num>347.00/0.00</num></td>
            <td></td>
            <td class="text-right"><b>Subtotal</b></td>
            <td width="100px"><num>P123,456,789.00</num></td>
            </tr>
            <!-- TOTAL ROW TOTAL ROW TOTAL ROW-->
            <tr>
                <td colspan="6"></td>
                <td class="text-right"><b>Discount</b></td>
                <td><num>P123,456,789.00</num></td>
            </tr>
            <!-- TOTAL ROW TOTAL ROW TOTAL ROW-->
            <tr>
                <td colspan="6"></td>
                <td class="text-right"><b>Grand Total</b></td>
                <td><num>P123,456,789.00</num></td>
            </tr>
        </table>
    </div>
    <div class="page_footer">
        <p><b>Checked By:</b> First Name Last Name Long Name</p>
        <p class="text-right float_right"><b>Received By:</b> First Name Last Name Long Name</p>
    </div>
</div>