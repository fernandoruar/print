<div id="page_container2">
    <div class="page_header">
        <div class="company2">
            <div class="company_logo2">
                <img src="public/images/logo.png" height="46px" width="auto">
            </div>
            <div class="company_name2"> 
                Company Name Company Long Name
            </div>

            <div class="information2">
                <table>
                    <tr>
                        <td width="80%" class="pL_5">Address: Long Address Sample Long Address Sample Long Address Sample</td>
                        <td class="text-right">SI No.: 1</td>
                    </tr>
                    <tr>
                        <td class="pL_5">Contact No.: 1234567890</td>
                        <td class="text-right">Date: 04/24/2018</td>
                    </tr>
                </table>
                <div class="page_num">
                    page 1/3
                </div>
            </div>

        </div>
    </div>
    <div class="table_container">
        <table width="100%">
            <tr>
                <td width="50%">Customer Name: <span>WILCON</span></td>
                <td>Contact No.: <span>1234567</span></td>
            </tr>
            <tr>
                <td>Address: <span>Address Sample Street City</span></td>
                <td>Memo: <span>SAMPLE MEMO MEMO LONG MEMO</span></td>
            </tr>
        </table>
        <table id="item_list">
            <tr class="thead">
                <td id="col_total_pieces">Total Pcs</td>
                <td id="col_product_name" colspan="2">Product Name, Color & Size</td>
                <td id="col_unit_price">Unit Price</td>
                <td id="col_amount">Amount</td>
                <td id="col_memo">Memo</td>
            </tr>
            <!-- ROW -->
            <tr>
                <td><num>1,010,000</num></td>
            <td rowspan="3">Product Name 001 Product Name Long Product </td>
            <td><cns>Ultramarine Blue</cns><cns>Extra Large</cns></td>
            <td><num>P88.00</num></td>
            <td><num>P88,880,000.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td><num>1,010,000</num></td>
            <td><cns>Crimson Red</cns><cns>Extra Large</cns></td>
            <td><num>P88.00</num></td>
            <td><num>P88,880,000.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td><num>1,010,000</num></td>
            <td><cns>Navy Blue</cns><cns>Extra Large</cns></td>
            <td><num>P88.00</num></td>
            <td><num>P88,880,000.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td><num>110</num></td>
            <td colspan="2">Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td><num>P88,000.00</num></td>
            <td><num>P9,680,000.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td><num>110</num></td>
            <td colspan="2">Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td><num>P88,000.00</num></td>
            <td><num>P9,680,000.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td><num>110</num></td>
            <td colspan="2">Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td><num>P88,000.00</num></td>
            <td><num>P9,680,000.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td><num>110</num></td>
            <td colspan="2">Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td><num>P88,000.00</num></td>
            <td><num>P9,680,000.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td><num>110</num></td>
            <td colspan="2">Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td><num>P88,000.00</num></td>
            <td><num>P9,680,000.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td><num>110</num></td>
            <td colspan="2">Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td><num>P88,000.00</num></td>
            <td><num>P9,680,000.00</num></td>
            <td></td>
            </tr>
            <!-- ROW -->
            <tr>
                <td><num>110</num></td>
            <td colspan="2">Product Name Long Product Name<cns>Ultramarine Blue-XL</cns></td>
            <td><num>P88,000.00</num></td>
            <td><num>P9,680,000.00</num></td>
            <td></td>
            </tr>
        </table>
        <table width="768px;">
            <!-- TOTAL ROW TOTAL ROW TOTAL ROW-->
            <tr>
                <td colspan="3" class="text-center" width="250px"><b>Total</b></td>
                <td><b>Pieces / Boxes</b></td>
                <td><num>347.00/0.00</num></td>
            <td></td>
            <td class="text-right"><b>Subtotal</b></td>
            <td width="100px"><num>P276,320,000.00</num></td>
            </tr>
            <!--TOTAL ROW TOTAL ROW TOTAL ROW-->
            <tr>
                <td colspan="6"></td>
                <td class="text-right"><b>Grand Total</b></td>
                <td><num>P275,320,000.00</num></td>
            </tr>
        </table>
    </div>
    <div class="page_footer">
        <div class="col_3">
            <div class="footer_data">
                <b>Encoded By:</b> <i>Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum</i>
            </div>
        </div>
        <div class="col_3">
            <div class="footer_data">
                <b>Checked By:</b> <i>Lorem Ipsum</i>
            </div>
        </div>
        <div class="col_3">
            <div class="footer_data">
                <b>Received By:</b> <i>Lorem Ipsum</i>
            </div>
        </div>
    </div>
</div>