<?php

$language = 1;

$datatablelanguage = "";
$username = "";
$password = "";
$signin = "";
$loginwarning = "";
$logout = "";
$invtitle = "";
$information = "";
$inventory = "";
$sales = "";
$otherexp = "";
$otherinc = "";
$reports = "";
$settings = "";
$establishment= "";
if (isset($_COOKIE["language"])) {
    $language = $_COOKIE["language"];
}

if ($language == 1) {
     $accounts = "Accounts";
    $username = "Username";
    $password = "Password";
    $signin = "Sign In";
    $loginwarning = "";
    $logout = "Logout";
    $invtitle = "Inventory System";
    $information = "Information";
    $inventory = "Inventory";
    $sales = "Sales";
    $otherexp = "Other Expenses";
    $otherinc = "Other Income";
    $reports = "Reports";
    $settings = "Settings";
    $datatablelanguage = '{"sProcessing": "Processing...","sLengthMenu": "Display _MENU_ records per page","sZeroRecords": "Nothing found - sorry","sInfo": "Showing page _PAGE_ of _PAGES_ (Total of _TOTAL_ entries)","sInfoEmpty": "No records available","sInfoFiltered": "(filtered from _MAX_ total records)","sInfoPostFix": "","sSearch": "Search:","sUrl": "","oPaginate": {"sFirst": "First","sPrevious": "Previous","sNext": "Next","sLast": "Last"}}';
    $username = "Username";
    $password = "Password";
    $signin = "Log in";
    $logout = "Log out";
    $loginwarning = "Invalid username/password!";
} else if ($language == 2) {
    $datatablelanguage = '{"sProcessing": "加载中...","sLengthMenu": "每页显示 _MENU_ 行","sZeroRecords": "对不起 没搜索到任何有关资料","sInfo": "共有 _TOTAL_ 个条目中找到. 显示 _PAGES_ 页中的第 _PAGE_ 页","sInfoEmpty": "没有相关资料","sInfoFiltered": "(已过滤 _MAX_ 行资料)","sInfoPostFix": "","sSearch": "搜索 :","sUrl": "","oPaginate": {"sFirst": "第一页","sPrevious": "上一页","sNext": "下一页","sLast": "最后一页"}}';
    $invtitle = "Inventory System";
    $username = "用户名";
    $password = "密码";
    $signin = "登陆";
    $logout = "登出";
    $loginwarning = "无效的账号/密码!";
    $information = "资料";
    $inventory = "库存";
    $sales = "销售";
    $otherexp = "额外支出";
    $otherinc = "其他收入";
    $reports = "报告";
    $settings = "Settings";
}