<?php

class model {

    function __construct() {
        
    }

    function estdata() {
        $aColumns = array('`id`', '`name`', '`address`', '`contact`', '`contactperson`', '`balance`', '');
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . ( $_GET['iDisplayStart'] ) . ", " .
                    ( $_GET['iDisplayLength'] );
        }


        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
				 	" . ( $_GET['sSortDir_' . $i] ) . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
		///Check Counter For Dynamic Search
        $sWhere = "";
		$check_count=0;
			$counter=0;
			$columns_shown=array();
			if(isset($_GET["show_cols"])&&$_GET["show_cols"]!="null"){
					$columns_shown=explode(",",$_GET["show_cols"]);
			}
			foreach($aColumns as $rows){
				if($_GET["bSearchable_".$counter]=="true"){
					if($_GET["sSearch_".$counter]!=""){
						$check_count++;
					}
				}
				$counter++;
			}
			if($check_count!=0){
				$sWhere = " WHERE (";
				for ($i = 0; $i < count($aColumns); $i++) {
					if($_GET["bSearchable_".$i]=="true"){
						if ($aColumns[$i] != ""&&$_GET["sSearch_".$i]!="") {
							if(count($columns_shown)>0){
								if(in_array($i,$columns_shown)>0){
										$sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch_'.$i] ) . "%' OR ";
								}else{}
							}else{
								$sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch_'.$i] ) . "%' OR ";		
							}
						}
					}
				}
				$sWhere = substr_replace($sWhere, "", -3);
				$sWhere .= ')';
			}

		
        $query = "SELECT * FROM (SELECT * FROM `estable`)asd";

        $array = prepareTable($query . " $sWhere $sOrder $sLimit", array());
        $count = count(prepareTable($query . " $sWhere", array()));

        $countfinal = $count;


        $finalarray = array();
        foreach ($array as $rows) {
            $button = "";


            $x = json_encode($rows);

			$input = "<div class='checkbox'><label><input type='checkbox' value='{$rows["id"]}' class='checkboxstyle chk-sm'><span class='lbl'></span></label></div>";
            $button = "<a data-toggle='tooltip' data-placement='bottom' title='Edit' ><button type='button'  data-toggle='modal' data-target='.ewarehouse' class='btn btn-flat btn-sm btn-primary'><i class='fa fa-edit'></i></button></a>
                                                                <a data-toggle='tooltip' data-placement='bottom' title='Deposit' ><button type='button' data-toggle='modal' data-target='.deposit' class='btn btn-flat btn-sm btn-success'><i class='fa fa-arrow-down'></i></button></a>
                                                                <a data-toggle='tooltip' data-placement='bottom' title='Delete' ><button type='button' data-toggle='modal' data-target='.dwarehouse' class='btn btn-flat btn-sm btn-danger'><i class='fa fa-trash-o'></i></button></a>";


            array_push($finalarray, array($input, $rows["name"], $rows["address"], $rows["contact"], $rows["contactperson"], $rows["balance"], $button));
        }

        $jsonarray = array("sEcho" => intval($_GET['sEcho']), "iTotalRecords" => $countfinal, "iTotalDisplayRecords" => $countfinal, "aaData" => $finalarray);
        return json_encode($jsonarray);
    }


	
	
    ////////////////////////////////////////////////////////////////////////////////
}
