<?php

class EmployeeModel{

    function __construct() {
        
    }

    function GetEmployeeData() {
        $aColumns = array('`image`', '`commsec_id`', '`name`', '`gender`', '`birthday`', '`employee_level`', '`first_name`', '', '');
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . ( $_GET['iDisplayStart'] ) . ", " .
                    ( $_GET['iDisplayLength'] );
        }


        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
				 	" . ( $_GET['sSortDir_' . $i] ) . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
		///Check Counter For Dynamic Search
        $sWhere = "";
		$check_count=0;
			$counter=0;
			$columns_shown=array();
			if(isset($_GET["show_cols"])&&$_GET["show_cols"]!="null"){
					$columns_shown=explode(",",$_GET["show_cols"]);
			}
			foreach($aColumns as $rows){
				if($_GET["bSearchable_".$counter]=="true"){
					if($_GET["sSearch_".$counter]!=""){
						$check_count++;
					}
				}
				$counter++;
			}
			if($check_count!=0){
				$sWhere = " WHERE (";
				for ($i = 0; $i < count($aColumns); $i++) {
					if($_GET["bSearchable_".$i]=="true"){
						if ($aColumns[$i] != ""&&$_GET["sSearch_".$i]!="") {
							if(count($columns_shown)>0){
								if(in_array($i,$columns_shown)>0){
										$sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch_'.$i] ) . "%' OR ";
								}else{}
							}else{
								$sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch_'.$i] ) . "%' OR ";		
							}
						}
					}
				}
				$sWhere = substr_replace($sWhere, "", -3);
				$sWhere .= ')';
			}

		
        $query = "SELECT * FROM (SELECT *,concat(`last_name`,', ',`first_name`,' ',`middle_name`) `name` FROM `employee`)asd";

        $array = prepareTable($query . " $sWhere $sOrder $sLimit", array());
        $count = count(prepareTable($query . " $sWhere", array()));

        $countfinal = $count;


        $finalarray = array();
        foreach ($array as $rows) {
            $button = "";


            $x = json_encode($rows);

			$input = "<div class='checkbox'><label><input type='checkbox' value='{$rows["id"]}' class='checkboxstyle chk-sm'><span class='lbl'></span></label></div>";
            $button = "<a data-toggle='tooltip' data-placement='bottom' title='Edit' ><button type='button'class='btn btn-flat btn-sm btn-primary' onclick='EditEmployee({$rows["id"]})'><i class='fa fa-edit'></i></button></a>
            <a data-toggle='tooltip' data-placement='bottom' title='Delete' ><button type='button' class='btn btn-flat btn-sm btn-danger' onclick='DeleteEmployee({$rows["id"]})'><i class='fa fa-trash-o'></i></button></a>";
            $contact = "<a data-toggle='tooltip' data-placement='bottom' title='Contacts' ><button type='button' class='btn btn-flat btn-sm btn-primary' onclick='EmployeeContact({$rows["id"]})'><i class='fa fa-phone'></i></button></a>";
			$image = "<a href='./public/images/employees/{$rows["image"]}' target='_blank'><img width='46px' src='./public/images/employees/{$rows["image"]}'></img></a>";
			$name = "<label id='lbl{$rows["id"]}'>{$rows["name"]}</label>";

            array_push($finalarray, array($rows["commsec_id"], $image, $name, $rows["gender"], $rows["birthday"], $rows["employee_level"], $rows["middle_name"], $contact, $button));
        }

        $jsonarray = array("sEcho" => intval($_GET['sEcho']), "iTotalRecords" => $countfinal, "iTotalDisplayRecords" => $countfinal, "aaData" => $finalarray);
        return json_encode($jsonarray);
    }
	
    function GetEmployeeByID($id) {
		$array = prepareTable("SELECT * FROM `employee` WHERE `id`=?",array($id));
		return json_encode($array);
	}
	
    function UpdateEmployee($id, $image, $employee){
		prepareTable("UPDATE `employee` SET `gender`=?, `commsec_id`=?, `last_name`=?, `first_name`=?, `middle_name`=?, `birthday`=?, `employee_level`=?, `image`='{$image}', `remarks`=? WHERE `id`='{$id}'",$employee);
		return json_encode(1);
	}
    function DeleteEmployee($id){
		$array = prepareTable("SELECT * FROM `employee` WHERE `id`=?", array($id));
		if(isset($array[0]["image"])&&$array[0]["image"]!=""){
			if(file_exists('./public/images/employees/'.$array[0]["image"])){
				unlink('./public/images/employees/'.$array[0]["image"]);
			}
		}
		prepareTable("DELETE FROM `employee` WHERE `id`=?",array($id));
		return json_encode(1);
	}
	
	
	
	/*Employee Contacts*/
	
    function GetEmployeeContacts($id) {
        $aColumns = array('`name`', '`contact`');
        $sLimit = "";
        if (isset($_GET['iDisplayStart']) && $_GET['iDisplayLength'] != '-1') {
            $sLimit = "LIMIT " . ( $_GET['iDisplayStart'] ) . ", " .
                    ( $_GET['iDisplayLength'] );
        }


        if (isset($_GET['iSortCol_0'])) {
            $sOrder = "ORDER BY  ";
            for ($i = 0; $i < intval($_GET['iSortingCols']); $i++) {
                if ($_GET['bSortable_' . intval($_GET['iSortCol_' . $i])] == "true") {
                    $sOrder .= $aColumns[intval($_GET['iSortCol_' . $i])] . "
				 	" . ( $_GET['sSortDir_' . $i] ) . ", ";
                }
            }

            $sOrder = substr_replace($sOrder, "", -2);
            if ($sOrder == "ORDER BY") {
                $sOrder = "";
            }
        }
		///Check Counter For Dynamic Search
        $sWhere = "";
		$check_count=0;
			$counter=0;
			$columns_shown=array();
			if(isset($_GET["show_cols"])&&$_GET["show_cols"]!="null"){
					$columns_shown=explode(",",$_GET["show_cols"]);
			}
			foreach($aColumns as $rows){
				if($_GET["bSearchable_".$counter]=="true"){
					if($_GET["sSearch_".$counter]!=""){
						$check_count++;
					}
				}
				$counter++;
			}
			if($check_count!=0){
				$sWhere = " WHERE (";
				for ($i = 0; $i < count($aColumns); $i++) {
					if($_GET["bSearchable_".$i]=="true"){
						if ($aColumns[$i] != ""&&$_GET["sSearch_".$i]!="") {
							if(count($columns_shown)>0){
								if(in_array($i,$columns_shown)>0){
										$sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch_'.$i] ) . "%' OR ";
								}else{}
							}else{
								$sWhere .= $aColumns[$i] . " LIKE '%" . ( $_GET['sSearch_'.$i] ) . "%' OR ";		
							}
						}
					}
				}
				$sWhere = substr_replace($sWhere, "", -3);
				$sWhere .= ')';
			}

		
        $query = "SELECT * FROM (SELECT A.*,B.`name` FROM `employee_has_contact` A LEFT JOIN `contact_type` B ON A.`id_contact_type`=B.`id` WHERE `id_employee`=?)asd";

        $array = prepareTable($query . " $sWhere $sOrder $sLimit", array($id));
        $count = count(prepareTable($query . " $sWhere", array($id)));

        $countfinal = $count;
        $finalarray = array();
        foreach ($array as $rows) {
            $button = "";
            $x = json_encode($rows);

            array_push($finalarray, array($rows["name"], $rows["contact"]));
        }

        $jsonarray = array("sEcho" => intval($_GET['sEcho']), "iTotalRecords" => $countfinal, "iTotalDisplayRecords" => $countfinal, "aaData" => $finalarray);
        return json_encode($jsonarray);
    }
	
	
	
}
